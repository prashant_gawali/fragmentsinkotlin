package com.example.fragmentskotlin

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment

class FragmentTwo : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val v = inflater.inflate(R.layout.fragment_two, container, false)
        val txt = v.findViewById<TextView>(R.id.tvsecondFragment)

        if (arguments?.getString("name") != null) {
            val str = arguments!!.getString("name")

            txt.setText(str)
            Log.d(TAG, "str: $str")
        }
        return v
    }

}