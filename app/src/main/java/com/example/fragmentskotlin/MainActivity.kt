package com.example.fragmentskotlin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    var isFragmentOneLoaded = true

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val first = FragmentOne()

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_holder, first)
            commit()
        }


    }

}