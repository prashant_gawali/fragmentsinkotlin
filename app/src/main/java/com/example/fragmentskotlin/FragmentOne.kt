package com.example.fragmentskotlin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment

class FragmentOne : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_one, container, false)


        val sendData = v.findViewById<Button>(R.id.sendData)
        val et = v.findViewById<EditText>(R.id.etName)
        sendData.setOnClickListener {

            val text = et.text.toString()
            if (text.isEmpty()) {
                Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show()

            } else {
                val fragmentTwo = FragmentTwo()

                val bundle = Bundle()
                bundle.putString("name", et.text.toString())
                fragmentTwo.arguments = bundle
                val manager = fragmentManager
                val transition = manager?.beginTransaction()

                transition?.replace(R.id.fragment_holder, fragmentTwo)
                transition?.addToBackStack(null)
                transition?.commit()
            }


        }


        return v
    }

}